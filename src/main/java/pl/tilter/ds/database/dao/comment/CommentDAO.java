package pl.tilter.ds.database.dao.comment;

import pl.tilter.ds.model.Comment;

import java.util.Set;

public interface CommentDAO {

    Comment create(Comment comment);
    Set<Comment> getAllComments();
}
