package pl.tilter.ds.database.dao.comment;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.tilter.ds.model.Comment;

import java.util.Set;

public class CommentDAOImpl implements CommentDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger("CommentDAOImpl");

    private static final SessionFactory sf;

    static {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure("hibernate.cfg.xml").build();

        Metadata metadata = new MetadataSources(registry).buildMetadata();

        sf = metadata.buildSessionFactory();
    }

    @Override
    public Comment create(Comment comment) {
        Session session = sf.openSession();
        LOGGER.info("Open session create comment");

        session.beginTransaction();
        session.save(comment);
        LOGGER.info("Comment created " + comment.getId());
        session.getTransaction().commit();

        LOGGER.info("Close session create comment");
        session.close();

        return comment;
    }

    @Override
    public Set<Comment> getAllComments() {
//        TODO
        return null;
    }
}
