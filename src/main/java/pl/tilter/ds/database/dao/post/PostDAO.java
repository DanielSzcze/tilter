package pl.tilter.ds.database.dao.post;

import pl.tilter.ds.model.Post;

import java.util.Set;

public interface PostDAO {

    Post create(Post post);
    Set<Post> getAllSortedByDate();
    Post findById(Long id);
}
