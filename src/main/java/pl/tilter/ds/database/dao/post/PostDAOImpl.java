package pl.tilter.ds.database.dao.post;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.tilter.ds.model.Post;

import java.util.HashSet;
import java.util.Set;

public class PostDAOImpl implements PostDAO{

    private static final Logger LOGGER = LoggerFactory.getLogger("PostDAOImpl");

    private static final SessionFactory sf;

    static {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure("hibernate.cfg.xml").build();

        Metadata metadata = new MetadataSources(registry).buildMetadata();

        sf = metadata.buildSessionFactory();
    }

    @Override
    public Post create(Post post) {

        Session session = sf.openSession();
        LOGGER.info("Open session create post");

        session.beginTransaction();
        session.save(post);
        session.getTransaction().commit();
        LOGGER.info("post created " + post.getId());

        session.close();
        LOGGER.info("Close session create post");

        return post;
    }

    @Override
    public Set<Post> getAllSortedByDate() {
        Set<Post> posts = new HashSet<>();

        Session session = sf.openSession();
        LOGGER.info("Open session for getAllSortedByDate");

        Query<Post> query = session.createQuery("select p FROM Post p LEFT JOIN FETCH p.comments", Post.class);
        query.getResultStream().forEach(posts::add);
        //TODO błąd nie wczytywania komentarzy
        LOGGER.info("save posts ".concat(posts.stream().findFirst().get().getComments().toString()));

        session.close();

        return posts;
    }

    @Override
    public Post findById(Long id) {
        Session session = sf.openSession();
        LOGGER.info("Open session findById");

        Post post = session.find(Post.class, id);

        LOGGER.info("Close session findById");
        session.close();

        return post;
    }
}
