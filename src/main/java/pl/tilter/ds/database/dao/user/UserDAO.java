package pl.tilter.ds.database.dao.user;

import pl.tilter.ds.model.User;

public interface UserDAO {

    User create (User user);
    User findByLogin (String login);
    User findById (Long id);
    User update(User user);
    User delete(int id);
}
