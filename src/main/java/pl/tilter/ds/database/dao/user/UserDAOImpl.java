package pl.tilter.ds.database.dao.user;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.tilter.ds.model.User;

public class UserDAOImpl implements UserDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger("UserDAOImpl");

    private static final SessionFactory sf;

    static {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure("hibernate.cfg.xml").build();

        Metadata metadata = new MetadataSources(registry).buildMetadata();

        sf = metadata.buildSessionFactory();
    }

    @Override
    public User create(User user) {

        Session session = sf.openSession();
        LOGGER.info("Open session create user");

        try {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
            LOGGER.info("After commit");
        } catch (RuntimeException e) {
            if (session.getTransaction() != null && session.getTransaction().isActive()) {
                session.getTransaction().rollback();
                LOGGER.warn("Rollback " + e.getMessage());
            }
        }

        session.close();
        LOGGER.info("Close session create");
        return user;
    }

    @Override
    public User findByLogin(String login) {

        User user = null;

        Session session = sf.openSession();
        session.beginTransaction();
        LOGGER.info("Open session findByLogin");
        try {
            Query<User> query = session.createQuery("FROM User where login = :login");
            query.setParameter("login", login);
            user = query.getSingleResult();
        } catch (RuntimeException e) {
            LOGGER.warn("User not found " + e.getMessage());
        }
        LOGGER.info("Close session findByLogin");
        session.close();

        return user;
    }

    @Override
    public User findById(Long id) {

        Session session = sf.openSession();
        session.beginTransaction();
        LOGGER.info("Open session findById");

        User user = session.find(User.class, id);

        LOGGER.info("Close session findById");
        session.close();

        return user;
    }

    @Override
    public User update(User user) {

        Session session = sf.openSession();
        session.beginTransaction();
        LOGGER.info("Open session update");

        session.update(user);
        session.getTransaction().commit();
        LOGGER.info(user.getPassword());

        session.close();
        LOGGER.info("Close session update");

        return user;
    }

    @Override
    public User delete(int id) {
        //TODO
        return null;
    }
}
