package pl.tilter.ds.services;

import pl.tilter.ds.database.dao.comment.CommentDAOImpl;
import pl.tilter.ds.database.dao.post.PostDAOImpl;
import pl.tilter.ds.database.dao.user.UserDAOImpl;
import pl.tilter.ds.model.Comment;
import pl.tilter.ds.model.Post;
import pl.tilter.ds.model.User;

public class CommentCreator {

    public Comment createComment(String context, Long userID, Long postID) {

        UserDAOImpl userDAO = new UserDAOImpl();
        PostDAOImpl postDAO = new PostDAOImpl();
        CommentDAOImpl commentDAO = new CommentDAOImpl();

        User user = userDAO.findById(userID);
        Post post = postDAO.findById(postID);

        Comment comment = new Comment(context, post, user);

        comment = commentDAO.create(comment);

        return comment;
    }
}
