package pl.tilter.ds.services;

import pl.tilter.ds.database.dao.post.PostDAOImpl;
import pl.tilter.ds.database.dao.user.UserDAOImpl;
import pl.tilter.ds.model.Post;
import pl.tilter.ds.model.User;

public class PostCreator {

    public Post createPost(String title, String context, Long userID) {

        UserDAOImpl userDAO = new UserDAOImpl();
        User user = userDAO.findById(userID);

        PostDAOImpl postDAO = new PostDAOImpl();

        Post post = new Post(title, context, user);

        postDAO.create(post);
        return post;
    }
}
