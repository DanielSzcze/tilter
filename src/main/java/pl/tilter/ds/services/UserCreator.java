package pl.tilter.ds.services;

import pl.tilter.ds.database.dao.user.UserDAOImpl;
import pl.tilter.ds.model.User;

public class UserCreator {

    public User createUser(String login, String email, String password) {
        User user = new User(login, email, password);

        UserDAOImpl userDAO = new UserDAOImpl();

        userDAO.create(user);
        return user;
    }
}
