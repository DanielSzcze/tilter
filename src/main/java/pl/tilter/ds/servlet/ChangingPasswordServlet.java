package pl.tilter.ds.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.tilter.ds.database.dao.user.UserDAOImpl;
import pl.tilter.ds.model.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ChangingPasswordServlet", value = "/changePassword")
public class ChangingPasswordServlet extends HttpServlet {

    private static final Logger LOGGER = LoggerFactory.getLogger("ChangePasswordServlet");

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("userId") != null) {
            LOGGER.info("change password for user " + req.getSession().getAttribute("userId"));
            goForward(req, resp, "/changePassword.jsp");
        } else {
            LOGGER.info("User not logged in GOTO login.jsp");
            goForward(req, resp, "/login.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("userId") != null) {
            LOGGER.info("USER login method POST");

            String oldPassword = req.getParameter("oldPassword");
            String newPassword = req.getParameter("newPassword");
            String newRePassword = req.getParameter("newRePassword");

            UserDAOImpl userDAO = new UserDAOImpl();
            User user = userDAO.findById(Long.valueOf(String.valueOf(req.getSession().getAttribute("userId"))));

            LOGGER.info(oldPassword + "        " + user.getPassword());
            oldPassword = User.codePassword(oldPassword);
            LOGGER.info(oldPassword + "        " + user.getPassword());

            if (oldPassword.equals(user.getPassword())) {
                LOGGER.info("Password valid");
                if (newPassword.equals(newRePassword)) {
                    LOGGER.info("New password valid");

                    newPassword = User.codePassword(newPassword);
                    user.setPassword(newPassword);
                    userDAO.update(user);

                    LOGGER.info("Password change");

                    req.getSession().setAttribute("Error", "0");
                    goForward(req, resp, "/profile.jsp");
                } else {
                    LOGGER.warn("New password not match");

                    req.getSession().setAttribute("Error", "2");
                    goForward(req, resp, "/changePassword.jsp");
                }
            } else {
                LOGGER.warn("Password not match");

                req.getSession().setAttribute("Error", "1");
                goForward(req, resp, "/changePassword.jsp");
            }
        } else {
            LOGGER.info("User not logged in GOTO login.jsp");

            goForward(req, resp, "/login.jsp");
        }
    }

    private void goForward(HttpServletRequest req, HttpServletResponse resp, String path) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(path);
        dispatcher.forward(req, resp);
    }
}
