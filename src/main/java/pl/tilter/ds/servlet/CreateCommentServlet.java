package pl.tilter.ds.servlet;

import pl.tilter.ds.database.dao.comment.CommentDAOImpl;
import pl.tilter.ds.database.dao.post.PostDAOImpl;
import pl.tilter.ds.database.dao.user.UserDAOImpl;
import pl.tilter.ds.model.Comment;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "CreateCommentServlet", value = "/createComment")
public class CreateCommentServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("userId") != null) {
            Long postId = Long.parseLong(req.getParameter("postId"));
            Long userId = Long.parseLong(String.valueOf(req.getSession().getAttribute("userId")));
            String context = req.getParameter("context");

            CommentDAOImpl commentDAO = new CommentDAOImpl();
            PostDAOImpl postDAO = new PostDAOImpl();
            UserDAOImpl userDAO = new UserDAOImpl();

            Comment comment = new Comment(context, postDAO.findById(postId), userDAO.findById(userId));

            commentDAO.create(comment);

            resp.sendRedirect(req.getContextPath() + "/");
        } else {
            goForward(req, resp, "/login.jsp");
        }
    }

    private void goForward(HttpServletRequest req, HttpServletResponse resp, String path) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(path);
        dispatcher.forward(req, resp);
    }
}
