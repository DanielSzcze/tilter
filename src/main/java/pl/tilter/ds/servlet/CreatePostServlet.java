package pl.tilter.ds.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.tilter.ds.database.dao.user.UserDAOImpl;
import pl.tilter.ds.model.Post;
import pl.tilter.ds.model.User;
import pl.tilter.ds.services.PostCreator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "CreatePostServlet", value = "/createPost")
public class CreatePostServlet extends HttpServlet {

    private static final Logger LOGGER = LoggerFactory.getLogger("CreatePostServlet");

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("userId") != null) {
            goForward(req, resp, "/createPost.jsp");
        } else {
            goForward(req, resp, "/login.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("userId") != null) {
            String title = req.getParameter("title");
            String context = req.getParameter("context");
            Long userID = Long.parseLong((String) req.getSession().getAttribute("userId"));

            LOGGER.info("create post");

            PostCreator postCreator = new PostCreator();
            Post post = postCreator.createPost(title, context, userID);

            UserDAOImpl userDAO = new UserDAOImpl();
            User user = userDAO.findById(userID);

            LOGGER.info("post created " + post.getId());
            LOGGER.info("posts: " + user.getPosts());

            req.getSession().setAttribute("posts", user.getPosts());
            goForward(req, resp, "/index.jsp");
        } else {
            goForward(req, resp, "/login.jsp");
        }
    }

    private void goForward(HttpServletRequest req, HttpServletResponse resp, String path) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(path);
        dispatcher.forward(req, resp);
    }
}
