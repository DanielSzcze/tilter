package pl.tilter.ds.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.tilter.ds.database.dao.user.UserDAOImpl;
import pl.tilter.ds.model.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "loginServlet", value = "/login")
public class LoginServlet extends HttpServlet {

    private static final Logger LOGGER = LoggerFactory.getLogger("LoginServlet");

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("userId") == null) {
            LOGGER.info("GOTO login form");
            goForward(req, resp, "/login.jsp");
        } else {
            LOGGER.info("User already logged");
            goForward(req, resp, "/index.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        password = User.codePassword(password);
        UserDAOImpl userDAO = new UserDAOImpl();

        User user = null;

        try {
            user = userDAO.findByLogin(login);
            LOGGER.info(password + "          " + user.getPassword());
        } catch (RuntimeException e) {
            LOGGER.error("USER not detected " + e.getMessage());
            req.getSession().setAttribute("Error", "1");
            goForward(req, resp, "/index.jsp");
        }

        if (user != null && user.getPassword().equals(password)) {
            req.getSession().setAttribute("userId", String.valueOf(user.getId()));
            req.getSession().setAttribute("login", user.getLogin());
            req.getSession().setAttribute("email", user.getEmail());
            req.getSession().setAttribute("Error", "0");

            LOGGER.info("USER detected " + user.getId() + "       " + user.getPassword());
            resp.sendRedirect(req.getContextPath() + "/");
        } else {
            LOGGER.error("Password not valid");
            req.getSession().setAttribute("Error", "2");
            goForward(req, resp, "/index.jsp");

        }
    }

    private void goForward(HttpServletRequest req, HttpServletResponse resp, String path) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(path);
        dispatcher.forward(req, resp);
    }
}
