package pl.tilter.ds.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.tilter.ds.database.dao.post.PostDAOImpl;
import pl.tilter.ds.model.Post;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;

@WebServlet(name = "MainServlet", value = "")
public class MainServlet extends HttpServlet {

    private static final Logger LOGGER = LoggerFactory.getLogger("MainServlet");

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("DO GET /");
        PostDAOImpl postDAO = new PostDAOImpl();

        Set<Post> posts = postDAO.getAllSortedByDate();
        req.getSession().setAttribute("posts", posts);
        goForward(req, resp, "/index.jsp");
    }

    private void goForward(HttpServletRequest req, HttpServletResponse resp, String path) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(path);
        dispatcher.forward(req, resp);
    }
}
