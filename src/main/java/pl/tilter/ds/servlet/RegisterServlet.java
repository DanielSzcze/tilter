package pl.tilter.ds.servlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.tilter.ds.model.User;
import pl.tilter.ds.services.UserCreator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "RegisterServlet", value = "/register")
public class RegisterServlet extends HttpServlet {

    private static final Logger LOGGER = LoggerFactory.getLogger("RegisterServlet");

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("GOTO /register.jsp by GET method");

        goForward(req, resp,"/register.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("In /register POST method ");

        String login = req.getParameter("login");
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        String rePassword = req.getParameter("retypePassword");

        UserCreator userCreator;
        User user = null;

        if (password.equals(rePassword)) {
            LOGGER.info("Password valid");
            userCreator = new UserCreator();
            user = userCreator.createUser(login, email, password);
            LOGGER.info("User created id: " + user.getId());
        } else {
            LOGGER.warn("Password not match");
            req.getSession().setAttribute("Error", "Reg-2");
            goForward(req, resp,"/register.jsp");
        }

        assert user != null;
        if (user.getId() != null) {
            req.getSession().setAttribute("userId", String.valueOf(user.getId()));
            req.getSession().setAttribute("login", user.getLogin());
            req.getSession().setAttribute("email", user.getEmail());
            req.getSession().setAttribute("password", user.getPassword());
            req.getSession().setAttribute("Error", "0");
            goForward(req, resp,"/index.jsp");
        } else {
            req.getSession().setAttribute("Error", "Reg-1");
            goForward(req, resp,"/register.jsp");
        }
    }

    private void goForward(HttpServletRequest req, HttpServletResponse resp, String path) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(path);
        dispatcher.forward(req, resp);
    }
}
