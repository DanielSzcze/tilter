create table users
(
	id int auto_increment,
	login varchar(25) not null,
	email varchar(50) not null,
	password varchar(40) not null,
	constraint users_pk
		primary key (id)
);
