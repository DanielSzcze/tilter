<%--
  Created by IntelliJ IDEA.
  User: danie
  Date: 18.07.2020
  Time: 09:30
  Project: Tilter_new
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="jstl.jsp"%>
<html>
<head>
    <title>Change password</title>
    <%@ include file="bootstrap4css.jsp"%>
</head>
<body>
<%@ include file="navbar.jsp"%>
    <h3>Zmiana hasła</h3>

    <c:if test="${Error == '1'}">
        Stare hasło nieprawidłowe
    </c:if>
    <c:if test="${Error == '2'}">
        Hasła nie są identyczne
    </c:if>

    <form action="changePassword" method="post">
        Stare hasło: <input type="password" name="oldPassword" required />
        Nowe hasło: <input type="password" name="newPassword" required />
        Powtórz nowe hasło: <input type="password" name="newRePassword" required />
        <input type="submit" value="Zmień hasło">
    </form>
<%@ include file="bootstrap4js.jsp"%>
</body>
</html>
