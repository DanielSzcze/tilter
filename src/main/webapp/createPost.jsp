<%--
  Created by IntelliJ IDEA.
  User: danie
  Date: 18.07.2020
  Time: 11:04
  Project: Tilter_new
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="jstl.jsp"%>
<html>
<head>
    <title>Create Post</title>
    <%@include file="bootstrap4css.jsp"%>
</head>
<body>
<%@include file="navbar.jsp"%>
    <form action="createPost" method="post">
        Tytuł: <input type="text" name="title" required />
        Treść: <input type="text" name="context" required />
        <input type="submit" value="Zapisz">
    </form>
<%@ include file="bootstrap4js.jsp"%>
</body>
</html>
