<%@ page import="pl.tilter.ds.model.Post" %>
<%@ page import="java.util.HashSet" %>
<%--
  Created by IntelliJ IDEA.
  User: daniel
  Date: 16.07.2020
  Time: 01:13
  Project: Tilter_new
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="jstl.jsp"%>
<html>
<head>
    <title>Tilter</title>
    <%@include file="bootstrap4css.jsp"%>
</head>
<body>
<%@include file="navbar.jsp"%>

<c:if test="${userId == null}">
    <h1 style="align-content: center">
        Witaj na Tilterze!  Zaloguj się lub zarejestruj.
    </h1>
    <form action="login" method="get">
        <input type="submit" name="Login" value="Logowanie" />
    </form>
    <form action="register" method="get">
        <input type="submit" name="Register" value="Rejestracja" />
    </form>
</c:if>
<c:if test="${userId != null}">
    <h1>
        Witaj ${login}
    </h1>
    <form action="profile" method="get">
        <input type="submit" value="Mój Profil" />
    </form>
    <form action="logout" method="get">
        <input type="submit" value="Wyloguj" />
    </form>

    <c:forEach var="post" items="${posts}">
        <div class="card w-75 mx-auto">
            <div class="row no-gutters">
                <div class="col-2 ">
                        <p class="text-center mt-3">${post.user.login}</p>
                </div>
                <div class="col-10">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-header">${post.title}</h5>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="col">
                            <p class="card-text">${post.context}</p>
                            <p class="card-text"><small class="text-muted">${post.time}  ${post.date}</small></p>
                        </div>
                    </div>
                    <div class="row justify-content-end mr-5">
                        <button type="button" class="btn btn-primary" data-toggle="collapse" data-target="#collapseComments${post.id}" aria-expanded="false" aria-controls="collapseComments${post.id}">
                            Wyświetl komentarze
                        </button>
                    </div>
                    <div class="collapse" id="collapseComments${post.id}">
                        <div class="card card-body">
                            <c:forEach var="comment" items="${post.comments}">
                                <div class="card w-75 offset-md-3 mx-auto">
                                    <div class="row no-gutters">
                                        <div class="col-2 ">
                                            <p class="text-center mt-3">${comment.user.login}</p>
                                        </div>
                                        <div class="col-7">
                                            <div class="card-body">
                                                <p class="card-text">${comment.context}</p>
                                                <p class="card-text"><small class="text-muted">${comment.time}  ${comment.date}</small></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                            <form action="createComment?postId=${post.id}" method="post">
                                <div class="form-row">
                                    <div class="col-9">
                                        <input type="text" class="form-control" id="formCreateCommentInput" name="context" required placeholder="Napisz komentarz..." />
                                    </div>

                                    <button type="submit" class="btn btn-primary">Dodaj</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%--<c:forEach var="comment" items="${post.comments}">
            <div class="card w-75 offset-md-3 mx-auto">
                <div class="row no-gutters">
                    <div class="col-2 ">
                        <p class="text-center mt-3">${comment.user.login}</p>
                    </div>
                    <div class="col-7">
                        <div class="card-body">
                            <p class="card-text">${comment.context}</p>
                            <p class="card-text"><small class="text-muted">${comment.time}  ${comment.date}</small></p>
                        </div>
                    </div>
                </div>
            </div>
        </c:forEach>--%>
    </c:forEach>
</c:if>
<%@ include file="bootstrap4js.jsp"%>
</body>
</html>
