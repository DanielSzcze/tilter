<%--
  Created by IntelliJ IDEA.
  User: danie
  Date: 12.07.2020
  Time: 21:13
  Project: Tilter
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="jstl.jsp"%>
<html>
<head>
    <title>Logowanie</title>
    <%@include file="bootstrap4css.jsp"%>
</head>
<body>
<%@include file="navbar.jsp"%>
    <c:if test="${Error == '1'}">
        Nie ma takiego użytkownika
    </c:if>
    <c:if test="${Error == '2'}">
        Błędne hasło
    </c:if>

    <form action="login" method="post">
        Login: <input type="text" name="login" /><br />
        Hasło: <input type="password" name="password" /><br />
        <input type="submit" value="Zaloguj" />
    </form>
<%@ include file="bootstrap4js.jsp"%>
</body>
</html>
