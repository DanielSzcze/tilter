<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="${pageContext.request.contextPath}/">Tilter</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item active">
                <a class="nav-link" href="${pageContext.request.contextPath}/profile">Profil <span class="sr-only">(curent)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="${pageContext.request.contextPath}/createPost">Utworz post</a>
            </li>
        </ul>
        <c:if test="${userId != null}">
            <div class="d-flex flex-column-reverse">
                Witaj, ${login}!
            </div>
            <form class="form-inline" action="logout" method="get">
                <button class="btn btn-dark btn-sm my-2 my-sm-0" type="submit">Wyloguj</button>
            </form>
        </c:if>
    </div>
</nav>
