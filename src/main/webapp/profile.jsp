<%--
  Created by IntelliJ IDEA.
  User: danie
  Date: 16.07.2020
  Time: 23:40
  Project: Tilter_new
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="jstl.jsp"%>
<html>
<head>
    <title>Profile</title>
    <%@include file="bootstrap4css.jsp"%>
</head>
<body>
<%@include file="navbar.jsp"%>
    <p>
        Nazwa użytkownika: ${login} <br />
        E-mail: ${email} <br />
    </p>
    <form action="createPost" method="get">
        <input type="submit" value="Stwórz post">
    </form>
    <form action="changePassword" method="get">
        <input type="submit" value="Zmiana hasła" />
    </form>
    <form action="logout" method="get">
        <input type="submit" value="Wyloguj" />
    </form>
<%@ include file="bootstrap4js.jsp"%>
</body>
</html>
