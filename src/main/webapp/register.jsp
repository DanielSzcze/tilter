<%--
  Created by IntelliJ IDEA.
  User: daniel
  Date: 16.07.2020
  Time: 01:14
  Project: Tilter_new
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="jstl.jsp"%>
<html>
<head>
    <title>Tilter - Rejestracja</title>
    <%@include file="bootstrap4css.jsp"%>
</head>
<body>
<%@include file="navbar.jsp"%>
<div class="container">
    <c:choose>
        <c:when test="${Error == 'Reg-1'}">
            <div class="alert alert-danger" role="alert">
                Błędne wartości!
            </div>
        </c:when>
        <c:when test="${Error == 'Reg-2'}">
            <div class="alert alert-danger" role="alert">
                Hasła nie są identyczne!
            </div>
        </c:when>
    </c:choose>

    <h2>Rejestracja</h2>

    <form action="register" method="post">
        <div class="form-group row">
            <div class="col-6">
                <input type="text" class="form-control" id="formGroupLoginInput" name="login" required placeholder="Login"/>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-6">
                <input type="text" class="form-control" id="formGroupEmailInput" name="email" required placeholder="Adres E-mail"/>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-3">
                <input type="password" class="form-control" id="formGroupPasswordInput1" name="password" required placeholder="Hasło"/>
            </div>
            <div class="form-group col-3">
                <input type="password" class="form-control" id="formGroupPasswordInput2" name="retypePassword" required placeholder="Powtórz hasło"/>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-auto">
                <button type="submit" class="btn btn-primary mb-2">Zarejestruj</button>
            </div>
        </div>
    </form>
</div>
<%@ include file="bootstrap4js.jsp"%>
</body>
</html>
